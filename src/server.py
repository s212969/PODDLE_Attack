from SSL import SSL
from settings import IV
from Crypto import Random
from Crypto.Cipher import AES
import socket
import selectors
import types

#Server settings
host = "127.0.0.1"  # Standard loopback interface address (localhost)
port = 8443  # Port to listen on (non-privileged ports are > 1023)

# Given a message check if its a valid SSLv3 message
# must be of correct size, correct padding and HMAC most be valid
def isMessageValid(message, key):
    if(len(message) % 16 != 0):
        print("Invalid message lenght!")
        return False

    if(SSL.isPaddingValid(message) == False):
        print("Invalid message padding!")
        return False
    
    if(SSL.isHMACValid(message, key) == False):
        print("HMAC check failed!")
        return False

    print("Message is valid!")
    return True

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind((host, port))
server.listen()
print(f"Listening on {(host, port)}")

while True:
    conn, addr = server.accept()
    from_client = b''

    #generate key for secure connection and send it to client
    key = Random.new().read( AES.block_size )
    conn.send(key)

    #wait for client HTTP request
    request = conn.recv(4096)

    #send HTTP response to client
    message = SSL.decryptWithKey(request, key, IV)
    if (isMessageValid(message, key)):
        # ... processing POST request ...
        conn.send(SSL.encryptWithKey("200 OK", key, IV))
    else:
        conn.send(SSL.encryptWithKey("500 Internal error", key, IV))

    #close connection
    conn.close()
    print('client disconnected')


