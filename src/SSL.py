"""Code for Exercise 3 on Exercise sheet 6."""
import base64

# You need the `pycryptodome` package for these modules
from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes
import hmac, hashlib, base64
from Crypto import Random
import socket
from settings import IV

BLOCK_SIZE = AES.block_size


class SSL:

    def __init__(self, ip, port):
        self.ip = ip
        self.port = port
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((ip, port))
        
        #simulate handshake, decide a common key to use for communication
        self.key = self.socket.recv(1024)
    
    def encrypt(self, message):
        return SSL.encryptWithKey(message, self.key, IV)

    def sendMessage(self, message):
        self.socket.sendall(message)
    
    def receiveMessage(self):
        data = self.socket.recv(1024)
        data = SSL.extractPayload(SSL.decryptWithKey(data, self.key, IV), self.key)
        return data

    def closeConnection(self):
        self.socket.close()
    
    def __del__(self):
        self.socket.close()

    class MessageTamperedException(Exception):
        pass

    # padding for the CBC cipher block
    @staticmethod
    def pad( s):
        return (BLOCK_SIZE - (len(s) % BLOCK_SIZE)) * chr((BLOCK_SIZE - (len(s) % BLOCK_SIZE)))

    # unpad after the decryption 
    # return the msg, the hmac and the hmac of msg 
    @staticmethod
    def __unpad_verifier(s, key):
        unpadded_msg = s[:-s[-1]] #remove padding
        hash_c = unpadded_msg[-32:] #extract hash
        msg = unpadded_msg[:-32] #extract message
        hash_d = hmac.new(key, msg, hashlib.sha256).digest()
        return msg, hash_d, hash_c

    @staticmethod
    def isPaddingValid(msg):
        if (msg[-1] >= 1 and msg[-1] <= 16): return True
        return False

    @staticmethod
    def isHMACValid(s, key):
        unpadded_msg = s[:-s[-1]] #remove padding
        hash_c = unpadded_msg[-32:] #extract hash
        msg = unpadded_msg[:-32] #extract message
        hash_d = hmac.new(key, msg, hashlib.sha256).digest()
        if (hash_d == hash_c): return True
        else: return False
    
    @staticmethod
    def extractPayload(s, key):
        unpadded_msg = s[:-s[-1]] #remove padding
        payload = unpadded_msg[:-32] #extract payload
        return payload

    # cipher a message
    @staticmethod
    def encryptWithKey(msg, key, iv):
        data = msg.encode()
        hash = hmac.new(key, data, hashlib.sha256).digest()
        padding = SSL.pad(data + hash)
        raw = data + hash + padding.encode()
        cipher = AES.new(key, AES.MODE_CBC, iv )
        return cipher.encrypt(raw) 

    @staticmethod
    def decryptWithKey(enc, key, iv):
        decipher = AES.new(key, AES.MODE_CBC, iv)
        msg = decipher.decrypt( enc )
        return msg